from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By

# Входной набор с параметрами для авторизации
# 0 - email, 1 - пароль, 2 - должна ли выполниться авторизация
inputDataArray = [
    [
        "nikita.developer97@gmail.com",
        "Test12345",
        True
    ],
    [
        "1234@gmail.com",
        "1234",
        False
    ],
    [
        "testing@gmail.com",
        "test1278",
        True
    ]
]

# Загружаем драйвер для работы с Google Chrome
driver = webdriver.Chrome()

# Переменные для подсчета кол-ва успешно выполненных и проваленных тесто
done = wrong = 0

# Открываем страницу авторизации
driver.get("https://litlife.club/forums")

# TODO Код тестирования писать здесь!

# Закрываем браузер
driver.close()
